﻿using SeleniumTest1.DAL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SeleniumTest1.Services;
using SeleniumTest1.Models;
using System.Net;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;
using SeleniumTest1;
using SeleniumTest1.Utils;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllData()
        {
            using (var dbContext = new CrawlerDbContext())
            {
                var productService = new ProductInfoService(dbContext);
                var products = productService.GetAll().ToList();
                products.ForEach(p => {
                    p.ImageFile = $"ProductData/Images/{p.ImageFile}";
                });

                // Reverse list
                products.Reverse();
                return Json(products, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Clear()
        {
            using (var dbContext = new CrawlerDbContext())
            {
                var productService = new ProductInfoService(dbContext);
                productService.RemoveAll();
            }

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReadPage(string url)
        {
            using (var dbContext = new CrawlerDbContext())
            {
                // Product info
                var product = new Product
                {
                    SiteName = new Uri(url).Host,
                    URL = url,
                    // TODO
                    ProductName = "NIN",
                };

                // Set flag
                var triggerService = new TriggerService(dbContext);
                triggerService.SetFetchFlag(FetchActionFlag.Start, JsonUtils.SerializeObject(product));

                // Then wait 45 times, 1s each time
                for (int i = 0; i < 45; i++)
                {
                    var flag = triggerService.GetFetchFlagFresh(out _);

                    // Finished
                    if (flag == FetchActionFlag.Stop)
                    {
                        // Reset flag
                        triggerService.SetFetchFlag(FetchActionFlag.Reset, JsonUtils.SerializeObject(new {}));

                        return Json("OK", JsonRequestBehavior.AllowGet);
                    }

                    // Wait 1 second
                    Thread.Sleep(1000);
                }

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Fetch action failed.");
            }
        }
    }
}